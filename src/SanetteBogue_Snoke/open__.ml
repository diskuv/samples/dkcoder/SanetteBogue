(* DkCoder suggestions *)
(* module Bogue = Tr1Bogue_Std.Bogue *)

(* Alerts.
   These were not identified as "missing" modules by codept
   since [Stdlib414Shadow] is part of runtime signatures
   and codept 0.12.0 does not use the module alerts. *)
module List = Tr1Stdlib_V414Base.List
module Array = Tr1Stdlib_V414Base.Array

let print_endline = Tr1Stdlib_V414Io.StdIo.print_endline

module Filename = Tr1Stdlib_V414CRuntime.Filename
module Printf = Tr1Stdlib_V414CRuntime.Printf
module Random = Tr1Stdlib_V414Random.Random
module Option = Tr1Stdlib_V414Base.Option

module Bogue = struct
  include Tr1Bogue_Std.Bogue

  module Theme = struct
    include Theme

    let find_share prog file =
      match prog with
      | "snoke" ->
          let share = Tr1Assets.LocalDir.v () in
          let f = Filename.concat share file in
          if Tr1Stdlib_V414CRuntime.Sys.file_exists f then Some share else None
      | _ ->
          find_share prog file
  end
end
