# DkCoder "SanetteBogue" library

This repository is a third party re-arrangement of [San Vũ Ngọc's](https://github.com/sanette) tutorials and demo source code to demonstrate how to make DkCoder scripts. Please see the original projects at <https://github.com/sanette> for licenses; Snoke in particular is GPL 3.

## Running

Using either a Unix shell or Windows PowerShell:

```sh
./dk SanetteBogue_Snoke.Snoke
```

## Maintaining Code

The source code is copied from <https://github.com/sanette> by the following script:

```sh
./dk user.vendor.snoke HELP
```
