# March 25, 2023 = commit f82ecbf2dcca20de5668f7c8f0b6dcb63cac8880
set(DEFAULT_SNOKE_VERSION f82ecbf2dcca20de5668f7c8f0b6dcb63cac8880)

function(help)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "" "MODE" "")
    if(NOT ARG_MODE)
        set(ARG_MODE FATAL_ERROR)
    endif()
    message(${ARG_MODE} "usage: ./dk user.vendor.snoke
    [VERSION <commit_or_tag>]
    [QUIET]

** Eventually this will be replaced with a DkCoder vendoring script. **

This script downloads Snoke from https://github.com/sanette/snoke and
re-arranges its code to conform with the DkCoder directory structure.

Examples
========

./dk user.vendor.snoke
    Downloads Snoke version ${DEFAULT_SNOKE_VERSION} from
    https://github.com/sanette/snoke and re-arranges its source code
    to conform with the DkCoder directory structure.

./dk user.vendor.snoke VERSION ${DEFAULT_SNOKE_VERSION}
    Downloads the specific version (git commit id or git tag) of
    Snoke.

Arguments
=========

HELP
  Print this help message.

VERSION <version>
  Downloads the specific Snoke version (git commit id or git tag).
  Defaults to version ${DEFAULT_SNOKE_VERSION}.

QUIET
  Do not print CMake STATUS messages.
")
endfunction()

function(run)
    # Get helper functions from this file
    include(${CMAKE_CURRENT_FUNCTION_LIST_FILE})

    set(noValues HELP QUIET)
    set(singleValues VERSION)
    set(multiValues)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "${noValues}" "${singleValues}" "${multiValues}")

    if(ARG_HELP)
      help(MODE NOTICE)
      return()
    endif()

    # QUIET
    if(ARG_QUIET)
        set(loglevel DEBUG)
        set(populate_QUIET_ARGS QUIET)
    else()
        set(loglevel STATUS)
        set(populate_QUIET_ARGS)
    endif()

    # VERSION
    if(ARG_VERSION)
        set(version "${ARG_VERSION}")
    else()
        set(version "${DEFAULT_SNOKE_VERSION}")
    endif()

    # Download
    include(FetchContent)
    FetchContent_Populate(
        snoke
        GIT_REPOSITORY https://github.com/sanette/snoke.git
        GIT_TAG "${version}"
        GIT_SHALLOW TRUE
        ${populate_QUIET_ARGS}
    )
    FetchContent_GetProperties(snoke)

    # Copy files
    if(CMAKE_VERSION VERSION_GREATER_EQUAL 3.26)
        set(copy_file_ARGS ONLY_IF_DIFFERENT INPUT_MAY_BE_RECENT)
    else()
        set(copy_file_ARGS ONLY_IF_DIFFERENT)
    endif()
    file(MAKE_DIRECTORY src/SanetteBogue_Snoke/assets__)
    file(COPY_FILE "${snoke_SOURCE_DIR}/LICENSE"                LICENSE ${copy_file_ARGS})
    file(COPY_FILE "${snoke_SOURCE_DIR}/src/CREDITS.org"        CREDITS-snoke.org ${copy_file_ARGS})
    file(COPY_FILE "${snoke_SOURCE_DIR}/src/snoke.ml"           src/SanetteBogue_Snoke/Snoke.ml ${copy_file_ARGS})
    file(COPY_FILE "${snoke_SOURCE_DIR}/src/levels.ml"          src/SanetteBogue_Snoke/Levels.ml ${copy_file_ARGS})
    file(COPY_FILE "${snoke_SOURCE_DIR}/src/splash.ml"          src/SanetteBogue_Snoke/Splash.ml ${copy_file_ARGS})
    file(COPY_FILE "${snoke_SOURCE_DIR}/src/SnakeChan-MMoJ.ttf" src/SanetteBogue_Snoke/assets__/SnakeChan-MMoJ.ttf ${copy_file_ARGS})
    file(COPY "${snoke_SOURCE_DIR}/src/images"      DESTINATION src/SanetteBogue_Snoke/assets__)
    file(COPY "${snoke_SOURCE_DIR}/src/sounds"      DESTINATION src/SanetteBogue_Snoke/assets__)
endfunction()
